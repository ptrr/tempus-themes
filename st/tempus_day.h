static const char *colorname[] = {
    "#3b2820",
    "#d62500",
    "#207f20",
    "#8a6900",
    "#306ad0",
    "#c6365d",
    "#00798d",
    "#f5f0f5",
    "#5b4440", 
    "#bb4b00", 
    "#5a7800",
    "#9a6033",
    "#6061d0",
    "#8850d0",
    "#007c6d",
    "#f8f1e0",
};

/* Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultbg = 15;
unsigned int defaultfg = 0;
static unsigned int defaultcs = 0;
static unsigned int defaultrcs = 15;
static unsigned int mousefg = 8;
static unsigned int mousebg = 7;

