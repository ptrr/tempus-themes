static const char *colorname[] = {
    "#1a1a1a",
    "#fb7e8e",
    "#4ab96d",
    "#b0a800",
    "#5aaaf2",
    "#ee80c0",
    "#55b1c3",
    "#c4bdaf",
    "#18143d", 
    "#f69d6a", 
    "#88c400",
    "#d7ae00",
    "#8cb4f0",
    "#de99f0",
    "#00ca9a",
    "#e0e0e0",
};

/* Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultbg = 0;
unsigned int defaultfg = 15;
static unsigned int defaultcs = 15;
static unsigned int defaultrcs = 0;
static unsigned int mousefg = 7;
static unsigned int mousebg = 8;

