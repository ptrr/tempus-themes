# Tempus Themes for Vim

The theme files can be copied manually or installed as a bundle with a plugin.

## Manual method: we use the command line

Start by cloning the `tempus-themes-vim` repo and entering it (unless you prefer copy-pasting, in which case adapt accordingly):

```sh
git clone https://gitlab.com/protesilaos/tempus-themes-vim.git --depth 1 && cd tempus-themes-vim
```


### Create destination directory

The manual method requires you to copy the file to `~/.vim/colors/`. Run the following command, while ignoring line that start with `#` (those are just comments):

```sh
# Create path to colors directory if it does not already exist
mkdir -p ~/.vim/colors/
```

### Copying to destination directory

Now run the following command for the theme of your choice. The example uses `tempus_winter.vim`:

```sh
cp colors/tempus_winter.vim ~/.vim/colors/
```

Done.

**Pro tip.** To copy all the themes at once, run the following:

```sh
cp colors/*.vim ~/.vim/colors/
```

### Applying the theme

Once copied to the right place, the theme is declared with the following options inside the `.vimrc`:

```vim
" Theme
syntax enable
colorscheme tempus_winter
```

## Plugin method

Use your favourite plugin manager. With [vim-plug](https://gitlab.com/junegunn/vim-plug) add the following line to your `.vimrc`:

```vim
Plug "protesilaos/tempus-themes-vim"
```

Then execute the plugin manager's command to update the plugin files.

### Applying the theme

Once available, the theme is declared with the following options inside the `.vimrc`:

```vim
" Theme
syntax enable
colorscheme tempus_winter
```

Enjoy!

## Additional resources

For all available ports, refer to the main [Tempus themes repo](https://gitlab.com/protesilaos/tempus-themes).

## Contributing

All contributions should be submitted to the Tempus themes generator. See [CONTRIBUTING.md](https://gitlab.com/protesilaos/tempus-themes-generator/blob/master/CONTRIBUTING.md).

## License

GNU General Public License Version 3. See [LICENSE](https://gitlab.com/protesilaos/tempus-themes-vim/blob/master/LICENSE).

## Meta

The Tempus themes project consists of a number of repositories. It is maintained with a set of scripts and utilities, stored in the [tempus-themes-utils](https://gitlab.com/protesilaos/tempus-themes-utils).

All theme files are created with the [Tempus themes generator](https://gitlab.com/protesilaos/tempus-themes-generator). See the generator CONTRIBUTING.md.

## Donations

If you appreciate this work, consider [donating via PayPal](https://www.paypal.me/protesilaos).
