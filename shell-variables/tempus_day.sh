#!/bin/bash
# Can be sourced from scripts to expose these colours as variables.

# theme: Tempus Day
# author: Protesilaos Stavrou (https://protesilaos.com)
# description: Light theme with warm colours (WCAG AA compliant)
background="#f8f1e0"
foreground="#3b2820"
cursorColor="#3b2820"
cursorColor2="#f8f1e0"
color0="#3b2820"
color1="#d62500"
color2="#207f20"
color3="#8a6900"
color4="#306ad0"
color5="#c6365d"
color6="#00798d"
color7="#f5f0f5"
color8="#5b4440"
color9="#bb4b00"
color10="#5a7800"
color11="#9a6033"
color12="#6061d0"
color13="#8850d0"
color14="#007c6d"
color15="#f8f1e0"
